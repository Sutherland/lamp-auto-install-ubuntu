
# How to AUTO Set up a LAMP Server on Ubuntu 18.04 (The Lazy Way)

### A Brief Explanation. 
I decided I would post this script as the amount of times I have set up LAMP servers over the years, not just for hosting sites but also as a pre-step to setting up a self hosted application. if you have done it once you'll realise how easy it is but when you have done it literally hundreds of times you'll appreciate the use of this script. 
### The Script.
The script itself will install Apache2, install MySQL and run the MySQL Secure Installation, Install and configure PHP. This script is compatible with any Ubuntu 16 & 18 release and will adjust the installation requirements depending on which OS is detected. There's not really a lot else to say except Enjoy!
### Get the Script.
```
cd ~
git clone https://gitlab.com/Sutherland/lamp-auto-install-ubuntu.git
```
### Run the Script.
```
cd ~/lamp-auto-install-ubuntu
sudo bash ./auto-lamp.sh
```
During the installation you will be asked to specify the password you would like set for the root MySQL user. On Ubuntu 16 you will be asked if you want to set your host name for Apache Configuration. 

If you read this and you would like to give any feedback or discuss the script any further, please email sutherland@scripting.online.
